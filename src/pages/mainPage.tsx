import { useEffect, useState } from "react";
import { Container, Row, Col, Card } from "react-bootstrap";

import Product from "../components/product";
import Cart from "../components/cart";
import { Pizza } from "../type/Pizza";
import axios from "axios";
import { toast } from "react-toastify";

const MainPage = () => {
  // State to store the list of pizzas and cart items

  const [pizzas, setPizzas] = useState<Pizza[]>([]);
  const [cartItems, setCartItems] = useState<Pizza[]>([]);

  // Fetch pizzas when the component mounts using useEffect
  useEffect(() => {
    fetchPizzas();
    
  }, []);

  //Function to fetch pizzas
  const fetchPizzas = async () => {
    try {
      const response = await axios.get("http://localhost:8080/pizza");
      toast.success("Feched all pizzas")
      console.log(response.data);
      setPizzas(response.data.data);
      
    } catch (error) {
      
      console.error("Error fetching pizzas:", error);
    }
   
  };

  //Function to add a pizza to the cart
  const addToCart = (
    pizza: Pizza,
    size: string,
    quantity: number,
    subTotal: number
  ) => {
    // if the pizza i already present in the cart
    const presentItem = cartItems.find(
      (item) => item.pizzaId === pizza.pizzaId && item.size === size
    );

    if (presentItem) {
      // if product present
      const updatedCartItems = cartItems.map((item) => {
        if (item.pizzaId === pizza.pizzaId && item.size === size) {
          return { ...item, quantity: item.quantity + quantity };
        }
        return item;
      });
      setCartItems(updatedCartItems);
    } else {
      // it product is not present in cart
      const updatedPizza: Pizza = { ...pizza, size, quantity, subTotal };
      setCartItems((prevCartItems) => [...prevCartItems, updatedPizza]);
    }
  };

  const removeItem = (pizza: Pizza) => {
    const updatedCartItems = cartItems.filter(
      (item) => item.pizzaId !== pizza.pizzaId
    );
    setCartItems(updatedCartItems);
  };

  return (
    <Container
      fluid
      style={{
        marginTop: "100px",
        paddingTop: "60px",
        backgroundColor: "#EEE",
      }}
    >
      <Row>
        <Col xs sm={8} lg={8}>
          <h3>Pizza</h3>
        </Col>
      </Row>
      <Row>
        <Col xs sm={8} lg={8}>
          <Card style={{ backgroundColor: "#EEE" }}>
            <Row>
              {pizzas.map((pizza) => (
                <Col key={pizza.pizzaId}>
                  <Product
                    key={pizza.pizzaId}
                    pizza={pizza}
                    addToCart={addToCart}
                  />
                </Col>
              ))}
            </Row>
          </Card>
        </Col>
        <Col xs sm={4} lg={4}>
          <Card style={{ backgroundColor: "#EEE" }}>
            <Cart cartItems={cartItems} removeItem={removeItem} />
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default MainPage;
