import { useEffect, useState } from "react";
import axios from "axios";
import { Order } from "../type/Order";
import { Button, Form, Modal } from "react-bootstrap";
import { Pizza } from "../type/Pizza";

// export interface OrderLine {
//   order_line_id: number;
//   order_id: number;
//   pizzaId: number;
//   quantity: number;
//   size: string;
//   total_price: number;
// }

const CreatedOrders = () => {
  const [orderDetail, setOrderDetail] = useState<Order[]>([]);
  // const [OrderLine, setOrderLine] = useState<OrderLine[]>([]);
  const [selectedOrderId, setSelectedOrderId] = useState<number | null>(null);
  const [showEditModal, setShowEditModal] = useState(false);
  const [updatedDeliveryAddress, setUpdatedDeliveryAddress] = useState("");
  const [updatedStatus, setUpdatedStatus] = useState("");
  // const [updatedPizzaList, setUpdatedPizzaList] = useState<Pizza[]>([]);
  const [updatedOrderId, setUpdatedOrderId] = useState<number>(0);
  const [updatedCustomerId, setUpdatedCustomerId] = useState<number>(0);

  useEffect(() => {
    fetchOrders();
  }, []);

  const fetchOrders = async () => {
    try {
      const response = await axios.get("http://localhost:8080/orders");

      console.log(response.data);
      setOrderDetail(response.data.data);
      console.log(response.data.data);

    } catch (error) {
      console.error("Error fetching orders:", error);
    }
  };

  // to get last 10 orders
  const latest10Orders = orderDetail.slice(-10);

  // to handle delete order button
  const handleDelete = async (orderId: number) => {
    const orderToDelete = latest10Orders.find(
      (order) => order.orderId === orderId
    );
    if (!orderToDelete) {
      alert("Order not found.");
      return;
    }

    try {
      const response = await axios.delete(
        `http://localhost:8080/orders/${orderId}`
      );
      if (response.status === 200) {
        setOrderDetail((prevOrders) =>
          prevOrders.filter((order) => order.orderId !== orderId)
        );
        alert("Order deleted successfully!");
      } else {
        throw new Error("Failed to delete the order");
      }
    } catch (error) {
      alert("Unable to Delete Order");
    }
  };

  const handleEdit = (orderId: number) => {
    setSelectedOrderId(orderId);

    const selectedOrder = orderDetail.find(
      (order) => order.orderId === orderId
    );
    if (selectedOrder) {
      // to set the initial value of feilds to the selected order
      setUpdatedStatus(selectedOrder.status);
      // setUpdatedPizzaList(selectedOrder.pizzaList);
      setUpdatedDeliveryAddress(selectedOrder.deliveryAddress);
      setUpdatedOrderId(selectedOrder.orderId);
      setUpdatedCustomerId(selectedOrder.customerId);
    }
  };

  //to edit order
  const handleUpdateOrder = async (updatedOrderData: {
    status: string;
    deliveryAddress: string;
   
  }) => {
    try {
      // to update the order
      const response = await axios.put(
        `http://localhost:8080/orders/${selectedOrderId}`,
        updatedOrderData
      );

      if (response.status === 200) {
        // Update the orderDetail state with the updated order
        setOrderDetail((prevOrders) =>
          prevOrders.map((order) =>
            order.orderId === selectedOrderId
              ? { ...order, ...updatedOrderData }
              : order
          )
        );
        // Reset the selected order id
        setSelectedOrderId(null);
        alert("Order updated successfully!");
      } else {
        throw new Error("Failed to update the order");
      }
    } catch (error) {
      alert("Unable to Update Order");
    }
  };
  const handleOpenEditModal = () => {
    setShowEditModal(true);
  };

  const handleCloseEditModal = () => {
    setShowEditModal(false);
  };

  return (
    <div
      style={{ display: "flex", justifyContent: "center", marginTop: "60px" }}
    >
      {latest10Orders.length > 0 && (
        <div>
          <h2
            style={{
              position: "fixed",
              marginBottom: "60px",
              marginLeft: "170px",
            }}
          >
            All Orders
          </h2>
          {latest10Orders.map((order) => (
            <div key={order.orderId} style={{ marginTop: "40px" }}>
              <div
                style={{
                  border: "1px solid #ccc",
                  width: "500px",
                  padding: "10px",
                  margin: "10px",
                }}
              >
                <div>
                  <p>Order Id:{order.orderId}</p>
                  <p>Status:{order.status}</p>
                  <p>Customer id:{order.customerId}</p>
                  <p>Delivery address:{order.deliveryAddress}</p>
                  {order.pizzaList.map((item) => (
                    <div key={item.pizzaId}>
                      <p>Name:{item.pizzaName}</p>
                      <p>size:{item.size}</p>
                    </div>
                  ))}

                  <p> Total Amount: ₹{order.totalAmount}</p>

                  <Button
                    variant="primary"
                    style={{ marginRight: "10px" }}
                    onClick={() => {
                      handleEdit(order.orderId);
                      handleOpenEditModal(); // Open the edit modal when the Edit button is clicked
                    }}
                  >
                    Edit
                  </Button>
                  <Button
                    variant="danger"
                    onClick={() => handleDelete(order.orderId)}
                  >
                    Delete
                  </Button>
                </div>
              </div>
            </div>
          ))}
        </div>
      )}

      {/* Edit Order Modal */}
      <Modal show={showEditModal} onHide={handleCloseEditModal}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Order</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group controlId="formOrderId">
              <Form.Label>Select Order Id to Edit</Form.Label>
              <Form.Control
                type="text"
                value={updatedOrderId}
                onChange={(e) => setUpdatedOrderId(parseInt(e.target.value))}
              ></Form.Control>
            </Form.Group>

            <Form.Group controlId="formDeliveryAddress">
              <Form.Label>Delivery Address</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter new delivery address"
                value={updatedDeliveryAddress}
                onChange={(e) => setUpdatedDeliveryAddress(e.target.value)}
              />
              {}
            </Form.Group>
            <Form.Group controlId="formStatus">
              <Form.Label>Status</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter new status"
                value={updatedStatus}
                onChange={(e) => setUpdatedStatus(e.target.value)}
              />
              <Form.Group controlId="customerId">
                <Form.Label>Customer id</Form.Label>
                <Form.Control
                  type="text"
                  value={updatedCustomerId}
                  onChange={(e) =>
                    setUpdatedCustomerId(parseInt(e.target.value))
                  }
                ></Form.Control>
              </Form.Group>
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseEditModal}>
            Close
          </Button>
          <Button
            variant="primary"
            onClick={() => {
              // Pass the updated order data to handleUpdateOrder function
              const updatedOrderData = {
                orderId: updatedOrderId,
                status: updatedStatus,
                deliveryAddress: updatedDeliveryAddress,
              };
              handleUpdateOrder(updatedOrderData);
              handleCloseEditModal();
            }}
          >
            Update
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default CreatedOrders;
