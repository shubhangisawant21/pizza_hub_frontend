import {
    BrowserRouter,
    Routes,
    Route,
  } from "react-router-dom";
  import MainPage from "../pages/mainPage";
import MainCart from "../pages/mainCart";
import CreatedOrders from "../pages/CreatedOrders";
import LoginPage from "../components/LoginPage";
import SignupPage from "../components/SignupPage";



const Content = () => {
    return(
    <>
        <BrowserRouter>
            <Routes>
                <Route path="/home" Component={MainPage}></Route>
                <Route path="/cart" Component={MainCart}></Route>
                <Route path="/orderlist" Component={CreatedOrders}></Route>
                <Route path="/login" Component={LoginPage}></Route>
                <Route path="/signup" Component={SignupPage}></Route>
            </Routes>
        </BrowserRouter>
    </>
    )
}

export default Content;