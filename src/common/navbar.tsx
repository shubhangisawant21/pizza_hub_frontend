
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import { Link } from 'react-router-dom';

const NavbarComponent = () => {
    return (
      <Navbar fixed="top" className="custom-navbar mb-5" style={{ backgroundColor: 'grey' }}>
        <Container>
          <Navbar.Brand href="/home">Pizza Palace</Navbar.Brand>
          <Navbar.Toggle />
          <Navbar.Collapse className="justify-content-end">
            <Navbar.Text>
             <a href="/login">Login</a>
             <a href="/signup" style={{marginLeft:'10px'}}>Sign Up</a>
              
            </Navbar.Text>
          
          
          </Navbar.Collapse>
        </Container>
      </Navbar>
    );
  };
  
  export default NavbarComponent;