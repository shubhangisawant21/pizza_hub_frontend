import axios from 'axios';
import React from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import { toast } from 'react-toastify';

interface Customer {
  firstName: string;
  lastName: string;
  address: string;
  phoneNo: string;
  emailId: string;
  password:string;
}

const SignupPage = () => {
  const { handleSubmit, control } = useForm<Customer>();

  const onSubmit = async (data: Customer) => {
    try {
      await axios.post('http://localhost:8080/customers', data);
      console.log('Customer data submitted:', data);
      toast.success("customer Successfully signed up");
    } catch (error) {
      console.error('Error submitting customer data:', error);
    }
  };

  return (
    <Container style={{ padding: '100px', maxWidth: '700px' }}>
      <Form onSubmit={handleSubmit(onSubmit)}>
        <Form.Group controlId="firstName">
          <Form.Label>First Name:</Form.Label>
          <Controller
            name="firstName"
            control={control} 
            defaultValue=""
            render={({ field }) => <Form.Control type="text" {...field} />}
          />
        </Form.Group>

        <Form.Group controlId="lastName">
          <Form.Label>Last Name:</Form.Label>
          <Controller
            name="lastName"
            control={control}
            defaultValue=""
            render={({ field }) => <Form.Control type="text" {...field} />}
          />
        </Form.Group>
         

        <Form.Group controlId="address">
          <Form.Label>Address:</Form.Label>
          <Controller
            name="address"
            control={control}
            defaultValue=""
            render={({ field }) => <Form.Control type="text" {...field} />}
          />
        </Form.Group>

        <Form.Group controlId="phoneNo">
          <Form.Label>Phone No:</Form.Label>
          <Controller
            name="phoneNo"
            control={control}
            defaultValue=""
            render={({ field }) => <Form.Control type="text" {...field} />}
          />
        </Form.Group>

        <Form.Group controlId="emailId">
          <Form.Label>Email ID:</Form.Label>
          <Controller
            name="emailId"
            control={control}
            defaultValue=""
            render={({ field }) => <Form.Control type="email" {...field} />}
          />
        </Form.Group>
        <Form.Group controlId="password">
          <Form.Label>Password:</Form.Label>
          <Controller
            name="password"
            control={control}
            defaultValue=""
            render={({ field }) => <Form.Control type="password" {...field} />}
          />
        </Form.Group>

        <Button type="submit" variant="primary" style={{ marginTop: '20px' }}>
          Sign Up
        </Button>
        <Button variant="secondary" style={{ marginLeft: '20px', marginTop: '20px' }} type="reset">
          Reset
        </Button>
      </Form>
    </Container>
  );
};

export default SignupPage;
