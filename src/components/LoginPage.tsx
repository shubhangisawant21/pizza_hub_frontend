// import React, { useState } from 'react';
// import { Button, Form } from 'react-bootstrap';
// import { useForm } from 'react-hook-form';
// import { toast } from 'react-toastify';
// import axios from 'axios';

// interface Customer {
//   firstName: string;
//   lastName: string;
//   address: string;
//   phoneNo: string;
//   emailId: string;
//   password: string;
// }

// const LoginPage = () => {
//   const { handleSubmit } = useForm<Customer>();
//   const [customer, setCustomer] = useState<Customer | null>(null);

//   const onSubmit = async (data: Customer) => {
//     try {
//       console.log('Form submitted with data:', data);
//       const response = await axios.get(`http://localhost:8080/customer?phoneNo=${data.phoneNo}`);

//       const customerDetails: Customer = response.data;
//       console.log('Customer details:', customerDetails);
//       setCustomer(customerDetails);
//       toast.success('Customer Logged in successfully');
//     } catch (error) {
//       console.error('Error fetching customer data:', error);
//       setCustomer(null);
//       toast.error('Customer not found or an error occurred while fetching customer data!');
//     }
//   };

//   return (
//     <div style={{ padding: '100px', maxWidth: '700px', margin: '0 auto' }}>
//       <Form onSubmit={handleSubmit(onSubmit)}>
//         <Form.Group controlId="phoneNo">
//           <Form.Label>Registered Mobile No:</Form.Label>
//           <Form.Control type="text" name="phoneNo" />
//         </Form.Group>
//         <Form.Group controlId="password" style={{ marginBottom: '20px' }}>
//           <Form.Label>Password:</Form.Label>
//           <Form.Control type="password" name="password" />
//         </Form.Group>

//         <Button type="submit" variant="primary">
//           Login
//         </Button>

//         {customer && (
//           <div>
//             <h2>Customer Details</h2>
//             <p>Name: {customer.firstName} {customer.lastName}</p>
//             <p>Address: {customer.address}</p>
//             <p>Email: {customer.emailId}</p>
//           </div>
//         )}
//       </Form>
//     </div>
//   );
// };

// export default LoginPage;
import React, { useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { toast } from 'react-toastify';
import axios from 'axios';

const LoginPage = () => {
  const [phoneNo, setPhoneNo] = useState('');
  const [password, setPassword] = useState('');
  const [customer, setCustomer] = useState(null);

  const handleSubmit = async (e: { preventDefault: () => void; }) => {
    e.preventDefault();
    try {
      console.log('Form submitted with data:', phoneNo, password);
      const response = await axios.get(`http://localhost:8080/customer?phoneNo=${phoneNo}`);

      const customerDetails = response.data;
      console.log('Customer details:', customerDetails);
      setCustomer(customerDetails);
      toast.success('Customer Logged in successfully');
    } catch (error) {
      console.error('Error fetching customer data:', error);
      setCustomer(null);
      toast.error('Customer not found or an error occurred while fetching customer data!');
    }
  };

  return (
    <div style={{ padding: '100px', maxWidth: '700px', margin: '0 auto' }}>
      <Form onSubmit={handleSubmit}>
        <Form.Group controlId="phoneNo">
          <Form.Label>Registered Mobile No:</Form.Label>
          <Form.Control
            type="text"
            name="phoneNo"
            value={phoneNo}
            onChange={(e) => setPhoneNo(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="password" style={{ marginBottom: '20px' }}>
          <Form.Label>Password:</Form.Label>
          <Form.Control
            type="password"
            name="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </Form.Group>

        <Button type="submit" variant="primary">
          Login
        </Button>

        {/* {customer && (
          <div>
            <h2>Customer Details</h2>
            <p>Name: {customer.firstName} {customer.lastName}</p>
            <p>Address: {customer.address}</p>
            <p>Email: {customer.emailId}</p>
          </div>
        )} */}
      </Form>
    </div>
  );
};

export default LoginPage;
