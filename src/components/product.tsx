import React, { useState,useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Pizza } from '../type/Pizza';


interface ProductProps {
  pizza: Pizza;
  addToCart: (pizza: Pizza, size: string, quantity: number,subTotal:number) => void;
}

const Product: React.FC<ProductProps> = ({ pizza, addToCart }) => {
  const [selectedSize, setSelectedSize] = useState('');
  const [selectedQuantity, setSelectedQuantity] = useState(1);
  const [subTotal, setSubTotal] = useState(0);


  const handleAddToCart = () => {
    addToCart(pizza, selectedSize, selectedQuantity,subTotal);
  };

  useEffect(() => {
    // Calculate the total price based on the selected size and quantity
    let calculatedPrice = 0;
    if (selectedSize === 'Regular') {
      calculatedPrice = pizza.priceRegularSize * selectedQuantity;
    } else if (selectedSize === 'Medium') {
      calculatedPrice = pizza.priceMediumSize * selectedQuantity;
    } else if (selectedSize === 'Large') {
      calculatedPrice = pizza.priceLargeSize * selectedQuantity;
    }
    console.log(subTotal);
    setSubTotal(calculatedPrice);
  }, [selectedSize, selectedQuantity]);

  return (
    <>
      <Card className='text-centre' style={{ width: '15rem', margin: '10px' }}>
        <Card.Img variant="top" src={pizza.imageUrl} alt="Pizza Image" />
        <Card.Body>
          <Card.Title>{pizza.pizzaName}</Card.Title>
          <p>{pizza.description}</p>

   
      <label htmlFor="size" ></label>
      <ul>
      
      <label>
        <input 
          type="checkbox"
          name="size"
          value="Regular"
          checked={selectedSize === 'Regular'}
          onChange={() => setSelectedSize('Regular')}
        />
        Regular <span>₹{pizza.priceRegularSize}</span>
      </label>
     
      <label>
        <input
          type="checkbox"
          name="size"
          value="Medium"
          checked={selectedSize === 'Medium'}
          onChange={() => setSelectedSize('Medium')}
        />
        Medium <span>₹{pizza.priceMediumSize}</span>
      </label>
      
      <label>
        <input
          type="checkbox"
          name="size"
          value="Large"
          checked={selectedSize === 'Large'}
          onChange={() => setSelectedSize('Large')}
        />
        Large <span>₹{pizza.priceLargeSize}</span>
      </label>
     
  </ul>

          <Button variant="primary" onClick={handleAddToCart} disabled={!selectedSize}>
            Add to Cart
          </Button>
        </Card.Body>
      </Card>
    </>
  );
};

export default Product;