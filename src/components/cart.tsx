import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Table from "react-bootstrap/Table";
import { Pizza } from "../type/Pizza";
import { FaTrash } from "react-icons/fa";
import axios from "axios";
import { Link } from "react-router-dom";

interface CartProps {
  cartItems: Pizza[];
  removeItem: (pizza: Pizza) => void;
  // orderDetail: Order[];
}

const Cart: React.FC<CartProps> = ({ cartItems, removeItem }) => {
  const [pizzaQuantities, setPizzaQuantities] = useState<{
    [pizzaId: number]: { [size: string]: number };
  }>({});

  const handleQuantity = (
    event: React.ChangeEvent<HTMLInputElement>,
    pizzaId: number,
    size: string
  ) => {
    const quantity = parseInt(event.target.value);

    setPizzaQuantities((prevQuantities) => {
      const updatedQuantities = { ...prevQuantities };
      const existingPizza = updatedQuantities[pizzaId];
      if (existingPizza) {
        updatedQuantities[pizzaId] = { ...existingPizza, [size]: quantity };
      } else {
        updatedQuantities[pizzaId] = { [size]: quantity };
      }
      return updatedQuantities;
    });
  };

  function createdOrder(): void {
    try {
      //setting order details
      const orderData = {
        customerId: 5,
        deliveryAddress: "Bhagyoday colony karvenagar",
        totalAmount: getTotalPrice(),
        status: "Pending",
        pizzaList: cartItems.map((item) => ({
          pizzaName: item.pizzaName,
          pizzaId: item.pizzaId,
          quantity: item.quantity,
          size: item.size,
          price: item.subTotal,
        })),
      };
      // API request to create the order
      axios
        .post("http://localhost:8080/orders", orderData)
        .then((response) => {
          console.log("Order created:", response.data);
          alert("Order created successfully!");
        })
        .catch((error) => {
          // Handle any errors that occurred during the order creation process
          console.error("Error creating order:", error);
        });
    } catch (error) {
      console.error("Error creating order:", error);
    }
  }

  //Total price of all pizza
  const getTotalPrice = (): number => {
    let totalPrice = 0;
    cartItems.forEach((item) => {
      const pizzaId = item.pizzaId;
      const size = item.size;
      const quantity = pizzaQuantities[pizzaId]?.[size] || 1;
      totalPrice += item.subTotal * quantity;
    });
    return totalPrice;
  };

  return (
    <Card style={{ margin: "10px" }}>
      <Card.Body>
        <Card.Title>Your Order</Card.Title>
        <Card.Text>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="130"
            height="130"
            fill="currentColor"
            className="bi bi-cart"
            viewBox="0 0 16 16"
          >
            <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
          </svg>

          {cartItems.length === 0 ? (
            <h5>Cart is empty. Add Products</h5>
          ) : (
            <div>
              <Table borderless>
                <thead>
                  <tr>
                    {/* <th>Image</th>
        <th>Pizza Name</th>
        <th>Size</th>
        <th>Quantity</th>
        <th>Subtotal</th>
        <th>Delete order</th> */}
                  </tr>
                </thead>
                <tbody>
                  {cartItems.map((item) => (
                    <tr
                      key={item.pizzaId}
                      style={{ borderTop: "1px solid #ccc" }}
                    >
                      <td style={{ borderBottom: "1px solid #ccc" }}>
                        <img
                          src={item.imageUrl}
                          alt={item.pizzaName}
                          style={{ width: "90px", height: "90px" }}
                        />
                      </td>
                      <td style={{ borderBottom: "1px solid #ccc" }}>
                        {item.pizzaName}
                      </td>
                      <td style={{ borderBottom: "1px solid #ccc" }}>
                        {item.size}
                      </td>
                      <td style={{ borderBottom: "1px solid #ccc" }}>
                        <input
                          id={`quantity-${item.pizzaId}`}
                          type="number"
                          value={
                            pizzaQuantities[item.pizzaId]?.[item.size] || 1
                          }
                          onChange={(event) =>
                            handleQuantity(event, item.pizzaId, item.size)
                          }
                          min={1}
                          style={{ width: "50px" }}
                        />
                      </td>
                      <td style={{ borderBottom: "1px solid #ccc" }}>
                        ₹
                        {item.subTotal *
                          (pizzaQuantities[item.pizzaId]?.[item.size] || 1)}
                      </td>

                      <td style={{ borderBottom: "1px solid #ccc" }}>
                        <button
                          style={{ backgroundColor: "red", color: "white" }}
                          onClick={() => removeItem(item)}
                        >
                          <FaTrash />
                        </button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </div>
          )}
        </Card.Text>

        <div>
          <span>
            Total Amount: <strong>₹{getTotalPrice()}</strong>
          </span>
        </div>

        <Button
          variant="primary"
          style={{ color: "white" }}
          onClick={createdOrder}
        >
          <b>create Order</b>
        </Button>
        <Link
          to="/orderlist"
          style={{
            textDecoration: "none",
            padding: "10px 20px",
            background: "#007bff",
            color: "#fff",
            borderRadius: "10px",
            fontWeight: "bold",
            marginLeft: "10px",
          }}
        >
          All Orders
        </Link>
      </Card.Body>
    </Card>
  );
};

export default Cart;
