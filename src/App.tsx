import './App.css';
import Content from './common/content';
import Footer from './common/footer';
import NavbarComponent from './common/navbar';
import { ToastContainer, Zoom, Flip,toast } from 'react-toastify';


function App() {
  return (
    <div className="App" style={{backgroundColor:"#EEE"}}>
        <NavbarComponent/>
        <Content/>
        <Footer/>
        <ToastContainer />
      
    </div>
  );
}

export default App;
