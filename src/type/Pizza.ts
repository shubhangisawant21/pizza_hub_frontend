export interface Pizza {
    
    pizzaId: number;
    pizzaName: string;
    description: string;
    pizzaType: string;
    quantity: number;
    imageUrl: string;
    priceRegularSize: number;
    priceMediumSize: number;
    priceLargeSize: number;
    size:string;
    price: number;
    subTotal:number
    totalPrice: number;
}