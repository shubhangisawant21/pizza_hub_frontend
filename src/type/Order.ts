import { Pizza } from "./Pizza";

export interface Order{
    
    orderId:number;
    status:string;
    orderDateTime:Date;
    totalAmount:number;
    deliveryAddress:string;
    customerId:number;
    pizzaList:Pizza[];
    timestamp:string;
}