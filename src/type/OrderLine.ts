export interface OrderLine{
    order_line_id:number;
    order_id:number;
    pizzaId:number;
    quantity: number;
    size:string;
    total_price:number;
}